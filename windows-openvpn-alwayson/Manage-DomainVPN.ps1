# Manage-DomainVPN.ps1
# (C) 2021 Nathaniel Roach
# Iterate through the active interfaces and see if Windows thinks it has Domain connectivity
# If we don't have domain connectivity, start up OpenVPN, if OpenVPN has an automatic connection
# Trigger this using Manage-OpenVPNOnNetworkConnection.xml in Task Scheduler

﻿$DomainNetworkReachable = $false

if (Get-ChildItem -Path 'C:\Program Files\OpenVPN\config-auto\' -filter *.ovpn) {
    try {
        Get-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -Newest 1 -ErrorAction Stop | Out-Null
    } catch {
        New-EventLog -LogName Application -Source "Manage-DomainVPN.ps1"
        Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 0 -EntryType Information -Message ("Initialised Manage-DomainVPN.ps1 event source")
    }

    Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 1 -EntryType Information -Message ("Beginning check for interfaces with domain connectivity")

    $OnlineInterfaces = (Get-NetAdapter | Where-Object {$_.Status -eq "up"})

    $OnlineInterfaces.Where{$_.InterfaceDescription -notlike "TAP-Windows*" -and $_.InterfaceAlias -notlike "Loopback *"} | ForEach-Object  {
        #$_.InterfaceAlias
        if (!$DomainNetworkReachable){
            if ((Get-NetConnectionProfile -InterfaceIndex $_.ifIndex -ErrorAction Ignore).NetworkCategory -eq "DomainAuthenticated") {
                Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 2 -EntryType Information -Message ("Found an active domain connection on interface " + $_.InterfaceAlias)
                #write-host "$_.InterfaceDescription - Network online"
                $DomainNetworkReachable = $true
                #break
            }
        }
    }

    if ($DomainNetworkReachable) {
        if ((Get-Service OpenVPNService).status -eq "Running"){
            Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 3 -EntryType Information -Message ("Connectivity found; Stopping OpenVPNService")
            Stop-Service OpenVPNService
        } else {
            Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 4 -EntryType Information -Message ("Connectivity found; No action required as OpenVPNService is already stopped")
        }
    } else {
        if ((Get-Service OpenVPNService).status -eq "Stopped"){
            Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 3 -EntryType Information -Message ("No connectivity found; Starting OpenVPNService")
            Start-Service OpenVPNService
        } else {
            Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 4 -EntryType Information -Message ("No connectivity found; No action required as OpenVPNService is already running")
        } 
    }

    $OVPNInterfaceIndex = (Get-NetAdapter -InterfaceDescription "TAP-Windows Adapter V9").ifIndex
    If ((Get-NetConnectionProfile -InterfaceIndex $OVPNInterfaceIndex).NetworkCategory -eq "Public") {
        Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 5 -EntryType Information -Message ("Setting the OpenVPN NIC as private...")
        Set-NetConnectionProfile -InterfaceIndex OVPNInterfaceIndex -NetworkCategory Private -ErrorAction Continue
    }
    Write-EventLog -LogName Application -Source "Manage-DomainVPN.ps1" -EventId 6 -EntryType Information -Message ("Done.")
}
