if (typeof crypto === 'undefined') {
  throw new Error('Crypto API is not supported. Please upgrade your web browser');
}

function genPasswordScrambled() {
  const crypto = window.crypto || window.msCrypto;

  const charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  const indexes = crypto.getRandomValues(new Uint32Array(12));

  let secret = '';

  for (const index of indexes) {
    secret += charset[index % charset.length];
  }
  document.getElementById("password").value = secret;
}

function genPasswordStructured() {
  const crypto = window.crypto || window.msCrypto;

  const charconsonants = 'bcdfghjklmnpqrstvwxyz';
  const charvowels = 'aeiou';
  const charsymbols = '@#$%';
  const charnums = '0123456789';

  const indexes = crypto.getRandomValues(new Uint32Array(14));

  let secret = '';

  secret += charconsonants[indexes[0] % charconsonants.length].toUpperCase();
  secret += charvowels[indexes[1] % charvowels.length];
  secret += charconsonants[indexes[2] % charconsonants.length];
  secret += charvowels[indexes[3] % charvowels.length];
  secret += charconsonants[indexes[4] % charconsonants.length];
  secret += charvowels[indexes[5] % charvowels.length];
  secret += charconsonants[indexes[6] % charconsonants.length];
  secret += charvowels[indexes[7] % charvowels.length];
  secret += charconsonants[indexes[8] % charconsonants.length];

  secret += charsymbols[indexes[9] % charsymbols.length];

  secret += charnums[indexes[10] % charnums.length];
  secret += charnums[indexes[11] % charnums.length];
  secret += charnums[indexes[12] % charnums.length];
  secret += charnums[indexes[13] % charnums.length];
  secret += '!'

  document.getElementById("password").value = secret;
}

function copyPassword() {
  var copyText = document.getElementById("password");
  copyText.select();
  document.execCommand("copy");
}

