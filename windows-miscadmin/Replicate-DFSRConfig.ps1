# Replicate-DFSRConfig.ps1
# (C) 2021 Nathaniel Roach
﻿# Force full AD replication, and then update DFS-R config

# https://www.powershellbros.com/list-domain-controllers-domain-report/

# Get your ad domain
$DomainName = (Get-ADDomain).DNSRoot
 
# Get all DC's
$DomainControllers = Get-ADDomainController -Filter * -Server $DomainName | Select-Object Hostname,Ipv4address,isglobalcatalog,site,forest,operatingsystem
 
$DomainControllers | ForEach-Object {
    Write-Host $_.Hostname
    Invoke-Command -ComputerName $_.Hostname {
        repadmin /syncall /force /APed
    }
}

$DFSRServers = (Get-DfsrMember | select -Unique -Property ComputerName)

$DFSRServers | Update-DfsrConfigurationFromAD -Verbose
