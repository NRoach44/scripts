# Update-RDCertificates.ps1
# (C) 2021, Nathaniel Roach
﻿# Find the latest Let's Encrypt cert and install it on the RD collection

#Requires -RunAsAdministrator
[cmdletbinding()]
Param(
    [Parameter(Mandatory=$false)] [Switch]$ForceUpdate = $false
    )

$ErrorActionPreference = "Stop"
$VerbosePreference = "Continue"

Import-Module -Name RemoteDesktop -Global

$CertsInstalled = (Get-RDCertificate)

$CertsInstalled | ForEach-Object {
    Write-Verbose ("Checking RD Role " + $_.Role)
    If ($_.ExpiresOn -lt (Get-Date).AddDays(-10)){
        Write-Warning ([string]$_.Role + " Certificate expiring soon! " + $_.ExpiresOn)
    }
}

$CertLatest = (Get-ChildItem -path cert:\LocalMachine\My | Where-Object {$_.Issuer -like "*let's encrypt*"} | Sort-Object -Property NotAfter -Descending | Select-Object -First 1)
Write-Verbose ("Found cert " + $CertLatest.Thumbprint)

# Check if cert is currently valid, stop if not
If ($CertLatest.NotAfter -lt (Get-Date)){
    Write-Error ("Found certificate is no longer valid. Install newer cert or adjust script's search. " + $CertLatest.Issuer + " " + $CertLatest.Subject + " " + $CertLatest.NotAfter)
}

# Check if cert is expiring soon, warn if so
If ($CertLatest.NotAfter -lt (Get-Date).AddDays(40)){
    Write-Warning ("Found certificate doesn't appear to be new " + $CertLatest.Issuer + " " + $CertLatest.Subject + " " + $CertLatest.NotAfter)
}

# Check if cert has FQDN
If ($CertLatest.Subject -notlike "*firstclassstrata.com.au*"){
    Write-Error ("Certificate does not have firstclassstrata.com.au domain name: " + $CertLatest.Subject)
}

try {
    Set-RDCertificate -Role RDGateway -Thumbprint $CertLatest.Thumbprint -Force
    Set-RDCertificate -Role RDPublishing -Thumbprint $CertLatest.Thumbprint -Force
    Set-RDCertificate -Role RDWebAccess -Thumbprint $CertLatest.Thumbprint -Force
    Set-RDCertificate -Role RDRedirector -Thumbprint $CertLatest.Thumbprint -Force
} catch {
    Write-Error ("Error applying certificate: " + $_.exception.tostring())
}
