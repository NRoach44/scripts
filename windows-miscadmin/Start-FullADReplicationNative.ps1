# Start-FullADReplicationNative.ps1
# (C) 2021 Nathaniel Roach
# Using Powershell native functions, ask all DCs to replicate.
﻿
$AllPartitions = $True

$myDomain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain();

$RWDCs = Get-ADDomainController -Filter * | Where-Object {$_.IsReadOnly -eq $false}

$RWDCs | ForEach-Object {

    $GADC = $_

    $dc = $myDomain.DomainControllers | Where {$_.Name -eq $GADC.HostName}

    $dcName = $dc.Name;

    $partitions = @();
    if ($AllPartitions) {
        $partitions += $dc.Partitions;
    } else {
        $partitions += ([ADSI]"").distinguishedName;
    }
    ForEach ($part in $partitions) {
        Write-Host "$dcName - Syncing replicas from all servers for partition '$part'"
        $dc.SyncReplicaFromAllServers($part, 'CrossSite')
    }
}
