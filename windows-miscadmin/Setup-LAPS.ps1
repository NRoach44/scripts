# Setup-LAPS.ps1
# (C) 2019 Nathaniel Roach
# Setup & Configure LAPS on a domain\
# GPO Import / export has been broken in my experience, that part will need tuning

﻿#!ps
$ErrorActionPreference = "Stop"

#Editable Options
$DeployedSoftwarePath = "\NETLOGON\LAPS"
$computersOU = "OU=OU,DC=DOMAIN,DC=Local"
$securityGroupsOU = "OU=GROUP_OU,DC=DOMAIN,DC=Local"
$securityGroupRO = "secAD_LAPS_RO"
$securityGroupRW = "secAD_LAPS_RW"

#The following should be left alone
$domain = (Get-ADDomain).DNSRoot
$LAPSMSIDestination = "\\" + $domain + $DeployedSoftwarePath

if (-not ([adsi]::Exists("LDAP://" + $computersOU))) {
    Write-Host "ERROR: computersOU variable references non-existent OU"
    return
    }

if (-not ([adsi]::Exists("LDAP://" + $securityGroupsOU))) {
    Write-Host "ERROR: securityGroupsOU variable references non-existent OU"
    return
    }


#check if folder exists
Copy-Item "$PSScriptRoot\LAPS.x64.msi" -Destination $LAPSMSIDestination
Copy-Item "$PSScriptRoot\LAPS.x86.msi" -Destination $LAPSMSIDestination

Start-Process C:\Windows\System32\msiexec.exe -ArgumentList "/i $PSScriptRoot\LAPS.x64.msi ADDLOCAL=ALL /qn" -wait

try { Get-ADGroup -Identity $securityGroupRO } catch {
    New-ADGroup -Name "AD LAPS Password Read-Only" -SamAccountName $securityGroupRO -GroupCategory Security -GroupScope Global -DisplayName "AD LAPS Password Read-Only" -Path $securityGroupsOU -Description "Allows reading the stored LAPS Password" }

try { Get-AdGroup -Identity $securityGroupRW } catch {
    New-ADGroup -Name "AD LAPS Password Read-Write" -SamAccountName $securityGroupRW -GroupCategory Security -GroupScope Global -DisplayName "AD LAPS Password Read-Write" -Path $securityGroupsOU -Description "Allows writing the stored LAPS Password/Expiry" }

#Schema work
Import-module admpwd.ps
Update-admpwdadschema
Find-AdmPwdExtendedRights -OrgUnit $computersOU

# - Confirm that only the groups that should be able to see the password are listed for each OU
Write-host "Listing permissions on AD now... ========================================="
Set-admpwdcomputerselfpermission -OrgUnit $computersOU
Set-AdmPwdReadPasswordPermission -OrgUnit $computersOU -Allowedprincipals secAD_LAPS_RO
Set-AdmPwdResetPasswordPermission -OrgUnit $computersOU -Allowedprincipals secAD_LAPS_RW

Read-Host "Confirm that the above permissions outputs matches what is expected (Press enter to continue)"

#Imports the GPO
New-GPO -Name "Computers: LAPS" -Comment "Installs and Manages LAPS"
Import-GPO -BackupGpoName "Computers: LAPS" -TargetName "Computers: LAPS" -Path "$PSScriptRoot\GPOTemplate"
New-GPLink -Name "Computers: LAPS" -Target "$computersOU" -LinkEnabled No
