# Setup-iSCSIDisk.ps1
# (C) 2022 Nathaniel Roach
# Script to setup/connect an iSCSI disk, useful for sever core
# Does not intialise / partition the disk

﻿$Server = "iSCSI HOST"
$UID = "servername"
$Sec = "aaaaaaaaaaaa"
$LUN = "iqn.domain:wssrv01"

Get-Service "MSiSCSI" | Start-Service -passthru | Set-Service -StartupType Automatic

New-IscsiTargetPortal -TargetPortalAddress $Server

Get-IscsiTarget

Connect-IscsiTarget –IsPersistent $True -TargetPortalAddress $Server -AuthenticationType ONEWAYCHAP -ChapUsername $UID -ChapSecret $Sec -NodeAddress $LUN
