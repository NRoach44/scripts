# Refresh-ComputerGroups
# (C) 2021 Nathaniel Roach
# Get all the members in a group (per AD, so it'll be up to date
# Connect to them and refresh their memberships
# Useful when you use computer groups to control other things like GPO or file access

﻿Get-ADGroupMember SERVERS | ForEach-Object {
    Write-Host $_.Name
    Invoke-Command -ComputerName $_.Name -ScriptBlock {
        klist -lh 0 -li 0x3e7 purge
        gpupdate /force
    }
}
