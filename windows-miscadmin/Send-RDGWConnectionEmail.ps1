# Send-RDGWConnectionEmail
# (C) 2019 Nathaniel Roach
# Trigger this using task scheduler on the event on line 7
# Will send an email to the admin when a new connection is made through the RDGW
# May have race / timing issues.

﻿$thing = get-winevent -computername localhost -logname "Microsoft-Windows-TerminalServices-Gateway/Operational" -maxevents 100 | `
    where { $_.ID -eq 302} | `
    Select-Object -First 1
    write-host "$($thing.TimeCreated) $($thing.Message)"
    Send-MailMessage -smtpserver "SMTP SERVER" -from "RDGW@DOMAIN" -to "ADMIN@DOMAIN" -subject "New RDP connection" -Body "$($thing.TimeCreated) $($thing.Message)"
