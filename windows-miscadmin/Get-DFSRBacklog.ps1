﻿# Shows DFSR backlog counts for a particular server
# (C) 2021 Nathaniel Roach
# Portions (code to get $ThisBacklog) from internet.
# Query a DFS-R server and get the backlog size per each connection with other servers.

[cmdletbinding()]
Param(
)

$VerbosePreference = "continue"

$DFSR_ServerToQuery = "WS-DC01"

$DFSR_Conns = $null
$DFSR_Conns = (Get-DfsrConnection -DestinationComputerName $DFSR_ServerToQuery)

$DFSR_Conns | ForEach-Object {

    $ThisBacklog = $null

    $ThisBacklog = (Get-DfsrBacklog  -Groupname $_.GroupName  -SourceComputerName $_.SourceComputerName -DestinationComputerName $DFSR_ServerToQuery -verbose 4>&1).Message.Split(':')[2]

    if (!$ThisBacklog){
        $ThisBacklog = [string]"None"
    } else {
        $ThisBacklog = [int]$ThisBacklog
    }

    Write-Host ($_.SourceComputerName + " RG: " + $_.GroupName + " Backlog: " + $ThisBacklog)

    if ($ThisBacklog -ne "None"){
        Write-Verbose ("Get-DfsrBacklog -Groupname " + $_.GroupName +" -SourceComputerName " + $_.SourceComputerName + " -DestinationComputerName " + $DFSR_ServerToQuery + " | Format-Table -Property index,FullPathName")
    }

}
