# Start-WSUSCleanupWithDeploymentFix.ps1
# (C) 2021 Nathaniel Roach
# Start the WSUS cleanup script (Invoke-WsusServerCleanup, available elsewhere online)
# If it errors in a particular way, remove that revision ID from the DB.

﻿$ErrorActionPreference = "Stop"

Try {
    Invoke-WsusServerCleanup -CleanupObsoleteUpdates
} catch {
    if (
        ($_.exception.tostring() -like "*deployed to a non dss target group*")
    )
    {
        #(($_.exception.toString())-split '\n')[0]
        $revisionID = (($_.exception.toString() -split '\n')[0]) -replace '[ ()\D\d]+\D+revisionid: (\d+) \D+','$1'
        #Write-Host "Clearing RevisionID" $revisionID
        #break
        #Start-Process -wait -FilePath "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\SQLCMD.EXE" -ArgumentList $argument
        Invoke-SqlCMD -ServerInstance "np:\\.\pipe\MICROSOFT##WID\tsql\query" -Database "SUSDB" -InputFile "D:\Data\WSUS\Scripts\Clear-WSUSBrokenUpdates.SQL" -Variable "revi=$revisionID"
    } else {
        Write-Error "Other error occurred:"
        Write-Host $_.exception.tostring()
    }
}
