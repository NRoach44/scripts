# Start-AutomaticServices.ps1
# (C) 2020 Nathaniel Roach
# Really simple snippet to find all services that should be running and start them
# Useful on slow hosts

get-service | where-object {$_.Status -eq "Stopped" -and $_.StartType -match "auto"} | Start-Service
