# Remove-WSUSDrivers.ps1
# (C) 2021 Nathaniel Roach
# Clean up after a "teachable moment" with WSUS and it's synchronisation options

 Param(
[string]$WsusServer = ([system.net.dns]::GetHostByName('localhost')).hostname,
[bool]$UseSSL = $False,
[int]$PortNumber = 8530
)
 
 [reflection.assembly]::LoadWithPartialName("Microsoft.UpdateServices.Administration") | out-null

 do {
     try {
        $wsus = [Microsoft.UpdateServices.Administration.AdminProxy]::GetUpdateServer($WsusServer,$UseSSL,$PortNumber);
        break
    } catch {
        Write-Warning "WSUS died :("
        Start-WebAppPool -Name WsusPool
        Write-Warning "Restarted WsusPool"
        Start-Sleep -Seconds 5
    }
} while ($true)

 $wsus.getupdates() | `
 Where {$_.UpdateClassificationTitle -eq 'Drivers'} | `
 ForEach-Object {
    $Retry = $true
    do {
        try {
            $wsus.DeleteUpdate($_.Id. UpdateID)
            Write-Host $_.Title removed
            $Retry = $False
        } catch {
            Write-Warning "WSUS died :("
            Start-WebAppPool -Name WsusPool
            Write-Warning "Restarted WsusPool"
            Start-Sleep -Seconds 5
        }
    } while ($Retry)
 }
