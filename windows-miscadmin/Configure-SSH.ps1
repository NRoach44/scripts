﻿$InstallFail = $false

try {
    Get-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
} catch {
    Write-Host Capability not available...
    $InstallFail = $true
}

if (!$InstallFail) {
    Try {
        Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
        Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

        Set-Service -Name sshd -StartupType 'Automatic'
        Start-Service sshd

        New-NetFirewallRule -Name sshd `
        -DisplayName 'OpenSSH Server (sshd)' `
        -Enabled True `
        -Direction Inbound `
        -Protocol TCP `
        -Action Allow `
        -LocalPort 22 `
        -Profile Domain
    } catch {
        Write-Host Install / configuration disabled
    }
}