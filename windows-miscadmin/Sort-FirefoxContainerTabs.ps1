# Sort-FirefoxContainerTabs
# (C) Pre 2021 someone else
# Modified to sort by name instead of colour (?)

﻿# Currently orders the Firefox Container menu by color, icon and then by name
# The line that reads 'Sort-Object -Property color, icon, name' can change that behavior
# THIS IS NOT TESTED, DO NOT USE THIS ON YOUR COMPUTER!!
# AGAIN THIS IS COMPLETELY UNTESTED!!
# I AM NOT RESPONSIBLE IF YOUR FIREFOX OR COMPUTER IS DAMAGED IN ANY WAY!!

Get-Process -ProcessName FireFox -ErrorAction SilentlyContinue |
    ForEach-Object {
    $null = $_.closemainwindow()
}

$index = 0
do {
    Start-Sleep -Seconds 2
    $index++
    if ($index -gt 30) {
        Write-Error "Unable to close Firefox - halting"
        break
    }
} while (Get-Process -ProcessName Firefox -ErrorAction SilentlyContinue)

Write-Warning 'Ordering Firefox Containers menu.'
Write-Warning 'Do not open Firefox until complete...'

$ContainersPath = "$env:APPDATA\Mozilla\Firefox\Profiles"
Get-ChildItem -Path $ContainersPath -File 'containers.json' -Recurse |
    ForEach-Object {
    $Json = Get-Content $_.FullName -Raw |
        ConvertFrom-Json
    $Json.identities = $Json.identities |
        Sort-Object -Property name
    $Json | ConvertTo-Json |
        Set-Content $_.FullName
}
'Ordering complete'
