# Get-ExpiringCerts
# Large portions taken from online
# Get all the certs on ALL servers in the domain, and then sort them by expiry date

﻿$Deadline = (Get-Date).addmonths(24)
$Servers = @((Get-ADComputer -Filter {OperatingSystem -Like "Windows Server*" -and Enabled -eq $True}).DNSHostName)
$expCerts = @()
foreach ($server in $servers){
    $error.Clear() # Clear the flag that something has broken
    Write-Output "Checking certificates on $($server)"

    $store = New-Object System.Security.Cryptography.X509Certificates.X509Store("\\$($server)\My","LocalMachine")
    try {
        $store.Open("ReadOnly")
    }
    catch {
        Write-Warning "Store Failed to open on $Server"
        continue
    }
    if (!$error) {
        $store.Certificates | % {
            if ($_.NotAfter -lt $deadline){

            $ExpiresIn = $($_.NotAfter) - $(Get-Date)
            $subjectCN = $($_.Subject).Split(',')[0].TrimStart("CN=")
            $issuerCN = $($_.Issuer).Split(',')[0].TrimStart("CN=")

            $output = @{
                'server' = $server
                'expires' = $(($_.NotAfter).ToString("dd/MM/yyyy hh:mm:ss"))
                'days' = $ExpiresIn.Days
                'subCN' = $subjectCN
                'issCN' = $issuerCN
                }    
            $expCerts += New-Object PsObject -Property $output
            Write-Host ("Added a cert:" + $output.server + ":" + $output.subCN + " " + $output.days)
            }
        }
        for ($i=0; $i -lt $error.Count; $i++) {
            $message = "[" + $server + "] " + $error[$i]
            Write-Event -message $message -errorLevel Error -source $MyInvocation.MyCommand.Name
        }
    }
}

$sortedCerts = $expCerts | Sort-Object days
$sortedCerts | ft
