# Manage-Telegraf
# (C) 2021 Nathaniel Roach

# Install Telegraf if it isn't installed, and if it is, update the config (if it needs it)

#Requires -RunAsAdministrator
[cmdletbinding()]
Param(
    [Parameter(Mandatory=$false)] [Switch]$ForceUpdate = $false,
    [Parameter(Mandatory=$false)] [Switch]$ForceConfig = $false,
    [Parameter(Mandatory=$false)] [Switch]$ForceBinary = $false
    )

Set-strictmode -version latest

$ErrorActionPreference = "Stop"

$TelegrafRemotePath = "\\Domain\SoftwareShare\Telegraf"
$TelegrafExe32 = "i386\Telegraf.exe"
$TelegrafExe64 = "Telegraf.exe"
$TelegrafConfig = "telegraf.conf"
$TelegrafLocalPath = "C:\Program Files\Telegraf"
$TelegrafServiceName = "telegraf"

$TelegrafConfigRemotePath = ($TelegrafRemotePath + "\" + $TelegrafConfig)
$TelegrafConfigLocalPath = ($TelegrafLocalPath + "\" + $TelegrafConfig)

If ($ForceUpdate) {
    $ForceConfig = $true
    $ForceBinary = $true
}

$IntConfigUpdated = $false
$IntBinaryUpdated = $false
$IntServiceRegistered = $false

$HostBitness = (gwmi win32_operatingsystem | select osarchitecture).osarchitecture
if ($HostBitness -ne "64-bit")
{
	$TelegrafRemoteExeFullPath = ($TelegrafRemotePath + "\" + $TelegrafExe32)
} else {
    $TelegrafRemoteExeFullPath = ($TelegrafRemotePath + "\" + $TelegrafExe64)
}

Write-Verbose ("Using the following EXE for service install: " + $TelegrafRemoteExeFullPath)

function Update-TelegrafConfig {
    Param(
    [Parameter(Mandatory=$false)]
    [Switch]$NoService = $false
    )
    if (! $IntConfigUpdated) {
        try {
            Write-Verbose "Copying config..."
            Copy-Item -Path $TelegrafConfigRemotePath -Destination $TelegrafLocalPath -Force
            $IntConfigUpdated = $true
            if (! $NoService) {
                Write-Verbose "Restarting Service"
                try {
                    Restart-Service $TelegrafServiceName
                } catch {
                    Write-Warning "Error restarting telegraf during Update-TelegrafConfig"
                }
            }
        } catch {
            Write-Error ("Error copying config: " + $_.Exception)
        }
    }
}

function Update-TelegrafBinary {
    Param(
    [Parameter(Mandatory=$false)]
    [Switch]$NoService = $false
    )
    if (! $IntBinaryUpdated) {
        try {        
            Write-Verbose "Copying binary..."
            Copy-Item -Path $TelegrafRemoteExeFullPath -Destination ($TelegrafLocalPath + "\telegraf.exe" + ".new") -Verbose
            
            if (! $NoService) {
                Write-Verbose "Stopping Service and swapping files"
                try {
                    Stop-Service $TelegrafServiceName
                    Move-Item -Path ($TelegrafLocalPath + "\telegraf.exe") -Destination ($TelegrafLocalPath + "\telegraf.exe" + ".old") -Verbose -Force
                    Move-Item -Path ($TelegrafLocalPath + "\telegraf.exe" + ".new") -Destination ($TelegrafLocalPath + "\telegraf.exe") -Verbose -Force

                    Write-Verbose "Starting service again..."
                    Start-Service $TelegrafServiceName
                } catch {
                    Write-Warning "Error restarting telegraf during Update-TelegrafBinary"
                }
            } else {   
                Move-Item -Path ($TelegrafLocalPath + "\telegraf.exe" + ".new") -Destination ($TelegrafLocalPath + "\telegraf.exe") -Verbose -Force
            }
            $IntBinaryUpdated = $true
        } catch {
            Write-Error ("Error copying binary: " + $_.Exception)
        }
    }
}

function Install-TelegrafService {
    Write-Verbose "Starting install..."

    try { # making the directory if it doesn't exist
        If (!(Test-Path -Path $TelegrafLocalPath -PathType Container)){
            Write-Verbose "Making directory"

            Remove-Item -Path $TelegrafLocalPath -ErrorAction Ignore
            New-Item -Path $TelegrafLocalPath -ItemType Directory
        }

        Update-TelegrafConfig -NoService:$true
        Update-TelegrafBinary -NoService:$true
                
    } catch {
        Write-Error ("Error copying files: " + $_.exception)
    }
    
    try { # registering the service
        & ($TelegrafLocalPath + "\telegraf.exe") --service install
        $IntServiceRegistered = $true
    } catch {
        Write-Error ("Error registring telegraf service: " + $_.Exception)
    }
    Start-Service Telegraf
}

Try { 
    Write-Verbose "Checking presence of Telegraf service"
    Get-Service "telegraf" -ErrorAction Stop | Out-Null
    Write-Verbose "Telegraf service already installed"

    Write-Verbose "Checking if binary needs updating, might take a second"
    #If ((Get-FileHash $($TelegrafLocalPath + "\telegraf.exe")).hash -ne (Get-FileHash $TelegrafRemoteExeFullPath).hash) {
    If (!(Test-Path ($TelegrafLocalPath + "\telegraf.exe"))) {
        Update-TelegrafBinary
    } elseif ((Get-Item ($TelegrafLocalPath + "\telegraf.exe")).LastWriteTimeUtc -lt (Get-Item $TelegrafRemoteExeFullPath).LastWriteTimeUtc -or $ForceBinary) {
        Write-Host "Binary being updated..."
        Update-TelegrafBinary
    }

    Write-Verbose "Checking if config need updating"
    If (!(Test-Path ($TelegrafConfigLocalPath))) {
        Update-TelegrafConfig
    } elseif ((Get-FileHash $TelegrafConfigLocalPath).hash -ne (Get-FileHash $TelegrafConfigRemotePath).hash -or $ForceConfig) {
        Write-Host "Config being updated..."
        Update-TelegrafConfig
    }

} catch [Microsoft.PowerShell.Commands.ServiceCommandException] {
    Write-Host "Telegraf service missing, beginning install"
    Install-TelegrafService
}

Write-Host "Done."
