# NRoach44\'s misc script collections

THis repo contains certain scripts I\'ve created, bodged, or wholesale copied.

## Warning
These scripts have probably been used in at least one particular situation, they may work as intended for you, however there is a good chance that something will be different between your environment and mine at the time I ran it.

Some scripts may have been unceremoniously redacted and have chunks missing, so there\'s a chance that I\'ve removed a section without removing all of the references to it, or the redactions don\'t follow the syntax of the format.

Most importantly, these scripts have not been quality checked by anyone in particular other than myself. They shouldn\'t do anything particularly destructive, but no promises.

This is to say:

**NO WARRANTY – OPEN SOURCE SOFTWARE
i. BECAUSE THE OPEN SOURCE SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE OPEN SOURCE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE OPEN SOURCE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE OPEN SOURCE SOFTWARE IS WITH CUSTOMER. IN THE EVENT THE OPEN SOURCE SOFTWARE PROVE DEFECTIVE, CUSTOMER ASSUMES THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.**

`https://www.lawinsider.com/clause/no-warranty-open-source-software`

## Sources

In almost all instances I have written or at least compiled the script myself, either from reference or original material. In instances where segments of the scripts have been taken from elsewhere, I have made attempts to credit the source, however some of these scripts can be more than 5 years old, and were not intended to be made public, so I may not have adequately linked the source.

In instances where the script or file was copied wholesale, the original credits / copyright (if present) will have been left intact.

## License
In the instance where there is no license in the file itself, the files are to be considered under the MIT license, to Nathaniel Roach, and the date of the latest commit on the file.

## Contents:

| Name  | Platform  | Description  |
| ------------ | ------------ | ------------ |
| nft-persistent  | Linux (systemd + nftables)  | systemd service to apply nftables + GeoIP block list  |
| port-forward  | Linux (bash, iptables / nftables)  | Script to set up port forwarding through a Linux machine acting as a router  |
| web-phplog  | Web (PHP)  | Simple log collection php script, can be used in scripts that make HTTP GET requests |
| web-pwgen  | Web (PHP)  | Simple JS password generation  |
| windows-greenshot-install  | Windows (Powershell)  | Install Greenshot and copy a default config to the default profile |
| windows-meshcentral-agent-install  |  Windows (bat)  | Install a MeshCentral Agent if it's missing |
| windows-miscadmin  | Windows (Powershell, TSQL)  | Miscellaneous Windows Powershell snippets  |
| windows-openvpn-alwayson | Windows (Powershell)  | Connect a OpenVPN connection when off the domain  |
| windows-sysmon-installupgrade | Windows (Powershell)  | Install, update and configure sysmon  |
| windows-telegraf-installupgrade | Windows (Powershell)  | Install, update and configure telegraf  |
| windows-enableMSUpdate  | Windows (VBS)  | Enable Microsoft Update on Windows 7  |
| windows-virtio-installer  | Windows (Powershell)  | Install the virtio drivers from the network, by copying them first  |
