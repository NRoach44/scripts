<?php
    // Retrieve the URL variables (using PHP).
    $job = preg_replace("/[^a-zA-Z0-9\-]/", "", $_GET['job']);
    $host = strtoupper(preg_replace("/[^a-zA-Z0-9\-]/", "", $_GET['host']));
    $stage = $_GET['stage'];
    $status = $_GET['status'];
    $msg = $_GET['msg'];
//    echo "Host: ".$host."\n";
//    echo "Current Stage: ".$stage."\n";
//    echo "Current Status: ".$status."\n";
//    echo "Detailed message: ".$msg."\n";

$log_time = date('Y-m-d h:i:sa');
$log_msg = $log_time.",".$host.",".$job.",".$stage.",".$status.",".$msg;

echo $log_msg."\n";
//wh_log("************** Start Log For Day : '" . $log_time . "'**********");
wh_log($job,$host,$log_msg);
//wh_log("************** END Log For Day : '" . $log_time . "'**********");

function wh_log($log_job,$log_host,$log_msg)
{
    $log_filename = "joblogs/".$log_job;
//    $log_filename = "log";
    if (!file_exists($log_filename))
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
        $log_file_data = $log_filename.'/'.$log_host;
        file_put_contents($log_file_data, "time,host,job,stage,status,message\n", FILE_APPEND);
    }
    $log_file_data = $log_filename.'/'.$log_host;
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
}

?>
