@echo off

REM (C) 2022 Nathaniel Roach
REM Check for, and if missing, install a specific MeshCentral Agent

SET CHECKFILE="C:\Program Files\<< agentCustomization.companyName >>\<< agentCustomization.serviceName >>\MeshAgent.exe"

SET INSTALLERPATH=\\Domain\SoftwareShare\meshAgengs
SET INSTALLER32=meshagent32-New_GPOInstalls.exe
SET INSTALLER64=meshagent64-New_GPOInstalls.exe
SET INSTALLERARGS="-fullinstall"

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

IF EXIST %CHECKFILE% (
  echo Already installed. Quitting.
) ELSE (  
  echo Installing...
  if %OS%==32BIT %INSTALLERPATH%\%INSTALLER32% %INSTALLERARGS%
  if %OS%==64BIT %INSTALLERPATH%\%INSTALLER64% %INSTALLERARGS%
)
