# Install-Greenshot
# (C) 2021 Nathaniel Roach

# Install Greenshot and populate a default config file so that it doesn't nag the user on login
# Probably worth iterating through user profiles to add the default config, if there's no existing config.
﻿
#Requires -RunAsAdministrator
$ErrorActionPreference = "Stop"

If (!(Test-Path -Path ("C:\Haven\Logs\") -PathType Container)) {
    New-Item -Path "C:\Haven\" -Name "Logs" -ItemType Directory
}

Write-Host "Checking for existence of Greenshot unistaller File..."
If (!(Test-Path -Path ("C:\Program Files\Greenshot\unins000.exe") -PathType Leaf)) {
    Write-Host "Not found, installing..."
    Start-Process -FilePath ($PSScriptRoot + "\Greenshot-INSTALLER-1.2.10.6-RELEASE.exe") -ArgumentList "/VERYSILENT /NORESTART /LOG=C:\Haven\Logs\GreenshotInstall.log"
}

If (!(Test-Path -Path ("C:\Users\Default\AppData\Roaming\Greenshot") -PathType Container)) {
    New-Item -Path "C:\Users\Default\AppData\Roaming" -Name "Greenshot" -ItemType Directory
}

Write-Host "Checking for existence of Greenshot default config File..."
If (!(Test-Path -Path ("C:\Users\Default\AppData\Roaming\Greenshot\Greenshot-defaults.ini") -PathType Leaf)) {
    Write-Host "Not found, copying..."
    Copy-Item ($PSScriptRoot + "\Greenshot-defaults.ini") -Destination ("C:\Users\Default\AppData\Roaming\Greenshot\Greenshot-defaults.ini")
}

try {
    Remove-Item ("C:\Users\Default\AppData\Roaming\Greenshot\Greenshot.ini")
} catch {
    Write-Verbose "Bad file does not exist"
}
