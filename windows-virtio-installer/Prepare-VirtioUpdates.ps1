﻿# Prepare-VirtioUpdates.ps1
# (C) 2023 Nathaniel Roach
# Copies the virtio-win-guest-tools installer to the server, copy a script to install that
# Then fire off the script to perform the update.
# This is necessary because the application will update the network driver, causing connectivity to drop


$Source      = "\\domain\dfs\softwareShare\virtio-updates"
$Destination = "C:\YourOrg\Software\virtio-updates"

$ScriptName  = "Install-VirtioUpdate.ps1"
$AppName     = "virtio-win-guest-tools.exe"

#Requires -RunAsAdministrator

$SourcePathScript = ($Source + "\" + $ScriptName)
$SourcePathApp    = ($Source + "\" + $AppName)

$DestPathScript   = ($Destination + "\" + $ScriptName)
$DestPathApp      = ($Destination + "\" + $AppName)


Set-strictmode -version latest

$ErrorActionPreference = "Stop"
# Create convenience logging function

function LogMessage{
    Param(
    [Parameter(Mandatory=$true)]  [int]$EventId,
    [Parameter(Mandatory=$false)] [String]$EntryType = "Information",
    [Parameter(Mandatory=$true)]  [String]$Message
    )

    Write-EventLog -LogName Application -Source $ScriptName -EventId $EventId -EntryType $EntryType -Message $Message

    switch ($EntryType){
        "Information" {Write-Host ($ScriptName + ": " + $Message)}
        "Warning" {Write-Warning ($ScriptName + ": " + $Message)}
        "Error" {Write-Error ($ScriptName + ": " + $Message)}
        Default {Write-Warning ("Check LogMessage EntryType (" + $EntryType + ")\r\n" + $ScriptName + ": " + $Message)}
    }
}

# Set up event source first

try {
    Get-EventLog -LogName Application -Source $ScriptName -Newest 1 -ErrorAction Stop | Out-Null
} catch {
    New-EventLog -LogName Application -Source $ScriptName
    LogMessage -EventId 0 -EntryType Information -Message ("Initialised " + $ScriptName + " event source")
}

# Perform initial checks

LogMessage -EventId 100 -EntryType Information -Message "Performing initial system checks"

If (!(Test-Path ($SourcePathScript))) {
    LogMessage -EventId 1 -EntryType Error -Message ("Check for script in source location failed - " + $SourcePathScript)
}
If (!(Test-Path ($SourcePathApp))) {
    LogMessage -EventId 1 -EntryType Error -Message ("Check for app in source location failed - " + $SourcePathApp)
}

# Prepare

LogMessage -EventId 101 -EntryType Information -Message "Checking local copies of files"

If (!(Test-Path $Destination)) {
    New-Item -Path $Destination -ItemType Directory
    LogMessage -EventId 2 -Message "Created working directory"
}

If (!(Test-Path ($DestPathScript))) {
    LogMessage -EventId 3 -Message "Copying script locally"
    Copy-Item -Path $SourcePathScript -Destination $DestPathScript
} elseif ((Get-FileHash $SourcePathScript).hash -ne (Get-FileHash $DestPathScript).hash) {
    LogMessage -EventId 4 -Message "Updating local copy of script"
    Copy-Item -Path $SourcePathScript -Destination $DestPathScript
}

If (!(Test-Path ($DestPathApp))) {
    LogMessage -EventId 5 -Message "Copying app locally"
    Copy-Item -Path $SourcePathApp -Destination $DestPathApp
} elseif ((Get-Command $SourcePathApp).FileVersionInfo.FileVersion -ne (Get-Command $DestPathApp).FileVersionInfo.FileVersion) {
    LogMessage -EventId 6 -Message "Updating local copy of app"
    Copy-Item -Path $SourcePathApp -Destination $DestPathApp
}

# Run the script?

$SecondStageApp = "C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe"
$SecondStageArg = ("-ExecutionPolicy bypass " + $DestPathScript)

LogMessage -EventId 102 -EntryType Information -Message (
"Starting script...
" + $SecondStageApp + " " + $SecondStageArg)

Start-Process -FilePath $SecondStageApp -ArgumentList $SecondStageArg

LogMessage -EventId 103 -EntryType Information -Message "Remote version of script finished."
