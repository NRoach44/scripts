﻿# Install-VirtioUpdate.ps1
# (C) 2023 Nathaniel Roach

# Installs the Windows virtio tools / drivers with a log file
# Logs things to console and Event Log


$VirtioRelease = "oss"
$VirtioVersion = "229"

$ExePath = "C:\YourOrg\Software\virtio-updates\virtio-win-guest-tools.exe"

$ReleaseVersion = ("0.1." + $VirtioVersion)

#Requires -RunAsAdministrator
Set-strictmode -version latest

$ErrorActionPreference = "Stop"
# Create convenience logging function

$ScriptName  = "Install-VirtioUpdate.ps1"
function LogMessage{
    Param(
    [Parameter(Mandatory=$true)]  [int]$EventId,
    [Parameter(Mandatory=$false)] [String]$EntryType = "Information",
    [Parameter(Mandatory=$true)]  [String]$Message
    )

    Write-EventLog -LogName Application -Source $ScriptName -EventId $EventId -EntryType $EntryType -Message $Message

    switch ($EntryType){
        "Information" {Write-Host ($ScriptName + ": " + $Message)}
        "Warning" {Write-Warning ($ScriptName + ": " + $Message)}
        "Error" {Write-Error ($ScriptName + ": " + $Message)}
        Default {Write-Warning ("Check LogMessage EntryType (" + $EntryType + ")\r\n" + $ScriptName + ": " + $Message)}
    }
}

# Set up event source first

try {
    Get-EventLog -LogName Application -Source $ScriptName -Newest 1 -ErrorAction Stop | Out-Null
} catch {
    New-EventLog -LogName Application -Source $ScriptName
    LogMessage -EventId 0 -EntryType Information -Message ("Initialised " + $ScriptName + " event source")
}

LogMessage -EventId 50 -EntryType Information -Message ("Local Install-VirtioUpdate.ps1 script running, host is " + $host.name)

$ExeVersion = (Get-Command $ExePath).FileVersionInfo.FileVersion

if ($ExeVersion -ne $ReleaseVersion){
    LogMessage -EventId 51 -EntryType Error -Message ("File is not the latest version (expected " + $ReleaseVersion + ", got " + $ExeVersion)
}

LogMessage -EventId 53 -EntryType Information -Message ("Installing virtio-win-guest-tools version " + $ExeVersion)

$LogPath=("C:\YourOrg\Logs\Virtio-" + $VirtioRelease + "-" + $VirtioVersion + "-" + (Get-Date).ToString("yyyy-MM-dd_hh-mm-ss") + ".txt")
$RemoteLogPath = ("\\" + (Get-WmiObject win32_computersystem).DnsHostName + "\C$\YourOrg\Logs\Virtio-" + $VirtioRelease + "-" + $VirtioVersion + "-" + (Get-Date).ToString("yyyy-MM-dd_hh-mm-ss") + ".txt")
LogMessage -EventId 54 -EntryType Information -Message ("Log for this run will be " + $RemoteLogPath)

if ($($host.Name -match 'console')){
    $Arguments = ("/install /quiet /log " + $LogPath)
} else {
    $Arguments = ("/install /passive /log " + $LogPath)
}

LogMessage -EventId 55 -EntryType Information -Message ("Starting " + $ExePath + " with arguments " + $Arguments)

$retval = (Start-Process -FilePath $ExePath -ArgumentList $Arguments -NoNewWindow -Wait -PassThru).ExitCode


LogMessage -EventId 99 -EntryType Information -Message ("Local Install-VirtioUpdate.ps1 finished. Return value of app was " + $retval)
